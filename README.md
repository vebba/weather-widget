# Weather Widget

100% PWA responsive Web/Mobile Application<br>
Searches weather forecast by city name.<br>
It stores the API results in normalized Redux State.<br>
Emotion + Tailwind CSS used for styling React Components.<br>
HMR enabled, JAM stack <br>

## Installation

```
npm install
```

## How to use it

First please refer how to add the KEY `first` down below.

```
npm start
```

```
npm run lambda
```

<br>
Runs the app in the development mode.<br>
The browser will open automatically
The page will reload if changes are made.<br><br>

```
npm run build
```

Builds the app for production to `dist` folder.<br>
It bundles the code for production<br>
The build is minified and the filenames are hashed.<br><br>

```
npm run serve
```

Serves the production build on port 7000<br><br>

```
npm run lint
```

Runs eslint for errors checks

## Testing with the KEY

You must rename the `.env.example` file in the root directory to
`.env` and place the valid KEY. If not available, get the API KEY following steps on this page :<br>
[https://openweathermap.org/appid](https://openweathermap.org/appid)

## Testing without the KEY

Change `USE_MOCKS` environment variable to `true` inside the `.env` file if an API KEY is not available then

```
npm start
```

## Unit Tests

Jest and Enzyme are being used for unit tests.

```
npm run test:watch
```

Launches the test runner in the interactive watch mode

## e2e Tests

Cypress is being used for `e2e` testing some basic test added

```
npm start
```

```
npm run cypress
```

## TODO

- simplify, refactor code
- more meaningful tests
- once the app gets bigger code splitting or .mjs
- focus blur management on mobile vs desktop
- different colors based to the weather conditions
- again more tests
- loading state
- styling
- error handling 404 silent
