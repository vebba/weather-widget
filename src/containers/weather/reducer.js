export default function weather(state = {}, action) {
  switch (action.type) {
    case 'REQUEST_API_DATA_SUCCESS':
      return {
        ...state,
        ...action.payload.data
      };
    default:
      return state;
  }
}
