import { connect } from 'react-redux';
import { WeatherList } from '../../components';

const mapStateToProps = state => {
  const { weather } = state;
  return {
    city: weather.readings,
    readings: weather.readings,
    days: weather.days
  };
};

export const Weather = connect(mapStateToProps)(WeatherList);
