export const GET_WEATHER_FOR_CITY = '@@SEARCH__GET_WEATHER';
export const UPDATE_SEARCH_INPUT = '@@SEARCH__UPDATE_CITY';
export const getWeatherForCity = city => ({
  type: GET_WEATHER_FOR_CITY,
  payload: city
});
export const updateSearchInput = city => ({
  type: UPDATE_SEARCH_INPUT,
  payload: city
});
