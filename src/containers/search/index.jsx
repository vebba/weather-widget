import { connect } from 'react-redux';
import { getWeatherForCity } from './actions';
import { SearchForm } from '../../components';
const mapDispatchToPros = dispatch => {
  return {
    onSubmit: city => dispatch(getWeatherForCity(city))
  };
};
const mapStateToProps = state => {
  return {
    message: state.search.message,
    isLoading: state.search.isLoading,
    isError: state.search.isError
  };
};
export const Search = connect(
  mapStateToProps,
  mapDispatchToPros
)(SearchForm);
