import { GET_WEATHER_FOR_CITY } from './actions';

export default function search(state = {}, action) {
  switch (action.type) {
    case GET_WEATHER_FOR_CITY:
      return { ...state, isLoading: true, isError: false };
    case 'REQUEST_API_DATA_SUCCESS':
      return { ...state, message: '', isLoading: false, isError: false };
    case 'REQUEST_API_DATA_FAILED':
      return {
        ...state,
        message: action.payload,
        isLoading: false,
        isError: true
      };
    default:
      return state;
  }
}
