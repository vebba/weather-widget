import React from 'react';
import { Search } from '../containers/search';
import { Weather } from '../containers/weather';
import { hot } from 'react-hot-loader/root';

import styled from '@emotion/styled';
import tw from 'tailwind.macro';

const MainWrapper = styled.main`
  ${tw`font-sans flex w-full h-full flex-col items-center bg-grey-darkest`}
`;

const App = () => (
  <MainWrapper>
    <Search />
    <Weather />
  </MainWrapper>
);
export default hot(App);
