import fetch from 'node-fetch';
import querystring from 'querystring';
import dotenv from 'dotenv';
dotenv.config();

exports.handler = async event => {
  const params = querystring.parse(event.body);
  const city = params.city || 'London';
  const url = `https://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${
    process.env.API_KEY
  }&units=metric`;
  return await fetch(url)
    .then(response => response.json())
    .then(data => ({
      statusCode: data.cod,
      body: JSON.stringify(data)
    }))
    .catch(error => console.log(error));
};
