import { call, put, takeLatest } from 'redux-saga/effects';

export const api = (url, city) =>
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    body: 'city=' + city
  }).then(response => response.json());

const getMock = () =>
  fetch('../../data/mock.json').then(response => response.json());

const days = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

const getDayForDate = date =>
  days[new Date(`${date.replace(/ /g, 'T')}Z`).getDay()];

const normalizeState = data => {
  const { list, city } = data;
  const result = {};

  const measurements = list.reduce((acc, current) => {
    return {
      ...acc,
      [current.dt]: { ...current, name: getDayForDate(current.dt_txt) }
    };
  }, {});

  const daysById = list.reduce((day, curr) => {
    const dayname = getDayForDate(curr.dt_txt);
    if (!day[dayname]) {
      day[dayname] = [];
    }
    day[dayname].push(curr.dt);
    return day;
  }, {});

  result.readings = {
    city: `${city.name}, ${city.country}`,
    byId: measurements,
    allIds: Object.keys(measurements)
  };

  result.days = {
    byId: daysById,
    allIds: Object.keys(daysById)
  };

  return result;
};

function* fetchApiData(action) {
  try {
    const response =
      process.env.USE_MOCKS === 'true'
        ? yield call(getMock)
        : yield call(api, '/.netlify/functions/weather', action.payload);
    const data = response;
    if (data.cod === 401) {
      alert(data.message);
    }
    if (data.cod === '404') {
      yield put({
        type: 'REQUEST_API_DATA_FAILED',
        payload: data.message
      });
    } else {
      yield put({
        type: 'REQUEST_API_DATA_SUCCESS',
        payload: {
          data: normalizeState(data)
        }
      });
    }
  } catch (e) {
    throw new Error(e);
  }
}

export default function* mainSaga() {
  yield takeLatest('@@SEARCH__GET_WEATHER', fetchApiData);
}
