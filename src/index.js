import React from 'react';
import ReactDOM from 'react-dom';
import '@babel/polyfill';
import 'whatwg-fetch';
import { Provider } from 'react-redux';
import storeInit from './store';
import App from './app';

const reduxStore = storeInit;

if (process.env.NODE_ENV === 'development') {
  const axe = require('react-axe');
  axe(React, ReactDOM);
}

ReactDOM.render(
  <Provider store={reduxStore}>
    <App />
  </Provider>,

  document.getElementById('app')
);

// expose store when run in Cypress
if (window.Cypress) {
  window.store = reduxStore;
}
if (process.env.NODE_ENV !== 'development') {
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker
        .register('/service-worker.js')
        .then(registration => {
          /* eslint-disable */
          console.log('SW registered: ', registration);
          /* eslint-enable */
        })
        .catch(registrationError => {
          /* eslint-disable */
          console.log('SW registration failed: ', registrationError);
          /* eslint-enable */
        });
    });
  }
}
