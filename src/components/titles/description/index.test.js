import React from 'react';
import { shallow } from 'enzyme';
import Description from '.';

describe('<Description/>', () => {
  it('should render without props', () => {
    const description = shallow(<Description />);
    expect(description).toMatchSnapshot();
  });
});
