import React from 'react';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
import PropTypes from 'prop-types';

const TempWrapper = styled.div`
  ${tw`text-grey-lightest font-thin leading-none w-full text-center pb-4`}
  font-size : 5rem;
`;
const Temperature = ({ temperature }) => {
  return (
    <TempWrapper>
      {Math.floor(temperature)}
      &deg;C
    </TempWrapper>
  );
};

Temperature.propTypes = {
  temperature: PropTypes.number.isRequired
};
Temperature.defaultProps = {
  temperature: 0
};

export default Temperature;
