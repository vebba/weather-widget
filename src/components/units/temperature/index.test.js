import React from 'react';
import { shallow } from 'enzyme';
import Temperature from './index';

describe('<Temperature/>', () => {
  it('should render without props', () => {
    const component = shallow(<Temperature />);
    expect(component).toMatchSnapshot();
  });
});
