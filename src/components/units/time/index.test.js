import React from 'react';
import { shallow } from 'enzyme';
import Time from './index';

describe('<Time/>', () => {
  it('should render without props', () => {
    const component = shallow(<Time />);
    expect(component).toMatchSnapshot();
  });
});
