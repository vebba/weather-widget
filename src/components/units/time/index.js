import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';

const TimeWrapper = styled.div`
  ${tw`font-sans flex flex-col items-center`}
  font-size: 0.6rem;
`;
const Hour = styled.div``;
const PaPm = styled.div``;

const Time = ({ time }) => {
  const timeNum = time.slice(11, 13);
  const hour = timeNum.split('')[0] !== '0' ? timeNum : timeNum.split('')[1];
  const pmam = timeNum.split('')[0] === '0' ? 'am' : 'pm';
  return (
    <TimeWrapper>
      <Hour>{hour === '0' ? 12 : hour}</Hour>
      <PaPm>{pmam}</PaPm>
    </TimeWrapper>
  );
};

Time.propTypes = {
  time: PropTypes.string.isRequired
};
Time.defaultProps = {
  time: '2019-04-11 09:00:00'
};

export default Time;
