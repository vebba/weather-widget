import React from 'react';
import { shallow } from 'enzyme';
import Icon from '.';

describe('<Icon />', () => {
  let icon;
  beforeEach(() => {
    icon = shallow(<Icon />);
  });

  it('should render without props', () => {
    expect(icon).toMatchSnapshot();
  });
});
