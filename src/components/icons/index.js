import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { icons } from '../../assets/icons';

const Wrapper = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};
  color: ${props => props.color};
`;
// `http://openweathermap.org/img/w/${id}.png`
const Icon = ({
  id = '01d',
  width = '30px',
  height = '30px',
  color = '#dae1e7'
}) => {
  return (
    <Wrapper width={width} height={height} color={color}>
      <svg viewBox="0 0 30 30" x="0px" y="0px">
        <path fill={color} d={icons[id]} />
      </svg>
    </Wrapper>
  );
};
Icon.propTypes = {
  id: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  color: PropTypes.string
};
export default Icon;
