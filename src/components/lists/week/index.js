import React from 'react';
import PropTypes from 'prop-types';
import { WeatherCard, TodayCard } from '../../cards';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';

const WeatherListWrapper = styled.div`
  ${tw`w-full px-1 flex-col max-w-xl`}
`;
const DaysWrapper = styled.div`
  ${tw`flex justify-around flex-col`}
`;

class WeatherList extends React.PureComponent {
  render() {
    const { days, readings } = this.props;

    return (
      <WeatherListWrapper>
        {days ? (
          <TodayCard
            city={readings.city}
            predictions={days.byId[days.allIds[0]].map(id => readings.byId[id])}
          >
            <DaysWrapper>
              {days.allIds.map((day, index) => {
                if (index > 0) {
                  return (
                    <WeatherCard
                      day={day}
                      key={day}
                      predictions={days.byId[day].map(id => readings.byId[id])}
                    />
                  );
                }
              })}
            </DaysWrapper>
          </TodayCard>
        ) : (
          <div />
        )}
      </WeatherListWrapper>
    );
  }
}
WeatherList.propTypes = {
  readings: PropTypes.object,
  days: PropTypes.object
};
WeatherList.displayName = 'WeatherList';
export default WeatherList;
