import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
import Icon from '../../icons';
import { Time } from '../../units';

const MainWrapper = styled.div`
  ${tw`flex flex-col items-center w-full`}
`;

const ListWrapper = styled.div`
  ${tw`flex items-end`}
`;

const ListItemWrapper = styled.div`
  ${tw`flex flex-col items-center pr-4 mb-4`}
`;

const TodayList = ({ predictions }) => {
  return (
    <MainWrapper>
      <ListWrapper>
        {predictions.map((item, index) => {
          return (
            <ListItemWrapper key={`list-item-${index}`}>
              <Icon id={item.weather[0].icon} width="20px" height="20px" />
              <div>{Math.round(item.main.temp)}&deg;</div>
              <Time time={item.dt_txt} />
            </ListItemWrapper>
          );
        })}
      </ListWrapper>
    </MainWrapper>
  );
};
TodayList.propTypes = {
  predictions: PropTypes.array.isRequired
};

export default TodayList;
