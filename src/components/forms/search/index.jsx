import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
import Icon from '../../icons';

const Form = styled.form`
  ${tw`flex w-full px-1 max-w-sm`}
`;
const SearchInput = styled.input`
  ${tw` my-0 bg-grey-light w-full appearance-none p-3 pl-4 text-grey-darkest text-xl rounded border-none font-light focus:border-none`}
`;
const SubmitButton = styled.label`
  ${tw`flex ml-3 text-grey-light items-center bg-grey-light rounded p-3 pl-2 cursor-pointer `}
`;
const Loader = styled.div`
  ${tw`text-grey-light tracking-wide italic `}
`;
const ErrorMessage = styled.p`
  ${tw`text-red tracking-wide italic`}
`;
const Spacer = styled.div`
  ${tw`h-8 flex items-center text-sm`}
`;
const MainTitle = styled.h1`
  ${tw`font-sans text-lg py-4 text-grey-light font-normal`}
`;
const onlyLetters = /([a-zA-Z-,]\s{0,1})/g;

class SearchForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.searchInput = React.createRef();
    this.state = { value: '' };
  }
  handleChange = e => {
    const inputText = e.target.value;
    if (inputText.match(onlyLetters)) {
      this.setState({
        value: inputText.match(onlyLetters).join('')
      });
    } else {
      this.setState({ value: '' });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    if (this.state.value !== '') {
      this.searchInput.current.blur();
      this.props.onSubmit(this.state.value.trim());
    } else {
      this.setState({ value: '' });
    }
  };

  render() {
    return (
      <>
        <MainTitle>Weather Widget</MainTitle>
        <Form onSubmit={e => this.handleSubmit(e)}>
          <SearchInput
            ref={this.searchInput}
            type="text"
            data-testid="SearchForm__input"
            id="search"
            value={this.state.value}
            onChange={this.handleChange}
            placeholder="city name ..."
            onFocus={e => (e.target.placeholder = '')}
            onBlur={e => (e.target.placeholder = 'city name ...')}
          />
          <SubmitButton
            htmlFor="search"
            data-testid="SearchForm__submit"
            onClick={e => this.handleSubmit(e)}
          >
            {'.'}
            <Icon id="search" color="#606F7B" />
          </SubmitButton>
        </Form>
        <Spacer>
          {this.props.isLoading && <Loader>loading ...</Loader>}
          {this.props.isError && (
            <ErrorMessage data-testid="SearchForm__error">
              {this.props.message}
            </ErrorMessage>
          )}
        </Spacer>
      </>
    );
  }
}

SearchForm.propTypes = {
  message: PropTypes.string,
  onSubmit: PropTypes.func,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool
};

export default SearchForm;
