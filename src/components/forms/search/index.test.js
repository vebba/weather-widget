import React from 'react';
import { shallow } from 'enzyme';
import SearchForm from './index';

const simulateInputChange = (target, input) => {
  target.simulate('change', { target: { value: input } });
};

describe('<SearchForm/>', () => {
  const comp = shallow(<SearchForm />);
  const searchInput = comp.find('[data-testid="SearchForm__input"]');
  it('should render without props', () => {
    expect(comp).toMatchSnapshot();
  });

  it('should accept letters and spaces only', () => {
    simulateInputChange(searchInput, 'Trabki Wielkie');
    expect(comp.instance().state.value).toBe('Trabki Wielkie');
  });
  it('should not accept numbers and special characters', () => {
    simulateInputChange(searchInput, ' 432432 234 234 234 213 !@£@£ @!£@!');
    expect(comp.instance().state.value).toBe('');
  });
  it('should call handleSubmit', () => {
    const wrapper = shallow(<SearchForm />);
    const button = wrapper.find('SubmitButton');
    const handleSubmit = jest.spyOn(wrapper.instance(), 'handleSubmit');

    simulateInputChange(searchInput, 'New York');
    button.simulate('click', { preventDefault: () => {} });

    expect(handleSubmit).toHaveBeenCalled();
  });
});
