import React from 'react';
import { shallow } from 'enzyme';
import TodayCard from './index';

describe('<TodayCard/>', () => {
  it('should render without props', () => {
    const card = shallow(<TodayCard />);
    expect(card).toMatchSnapshot();
  });
});
