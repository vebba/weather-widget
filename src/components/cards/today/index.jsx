import React from 'react';
import PropTypes from 'prop-types';
import { Temperature } from '../../units/';
import Icon from '../../icons';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
import { TodayList } from '../../lists';

const TodayCardWrapper = styled.div`
  ${tw`font-sans rounded shadow-md text-grey-light mb-4 pt-4 flex flex-col lg:flex-row md:justify-between items-center `}
  background-color: rgba(18,40,58,0.5);
`;
const CityTitle = styled.h1`
  ${tw`text-3xl text-grey-dark font-normal capitalize`}
`;
const TodayWrapper = styled.div`
  ${tw`flex flex-col w-full items-center mb-4`}
`;
const DaysWrapper = styled.div`
  ${tw`flex flex-col w-full`}
`;
const Description = styled.p`
  ${tw`text-sm tracking-wide`}
`;
const IconWrapper = styled.div`
  ${tw`flex flex-col w-full justify-between items-end`}
`;
const Today = styled.label`
  ${tw`w-full text-center pb-1`}
`;
const Clock = styled.div`
  ${tw`w-full text-center pb-3 text-sm`}
`;

const TodayCard = ({ children, ...props }) => {
  const { predictions, city } = props;
  if (!predictions.length) {
    return <span>No items in the list</span>;
  } else {
    const data = predictions[0];
    const date = new Date();
    return (
      <TodayCardWrapper>
        <TodayWrapper>
          <CityTitle>{city}</CityTitle>
          <Description>{data.weather[0].description}</Description>
          <Icon id={data.weather[0]['icon']} width={'50%'} height={'50%'} />

          <IconWrapper>
            <Temperature temperature={data.main['temp']} />
            <Today>Today</Today>
            <Clock>{`${date.getHours()}:${date.getMinutes()}`}</Clock>
            <TodayList predictions={predictions} />
          </IconWrapper>
        </TodayWrapper>

        <DaysWrapper>{children}</DaysWrapper>
      </TodayCardWrapper>
    );
  }
};

TodayCard.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  predictions: PropTypes.array,
  city: PropTypes.string
};

TodayCard.defaultProps = {
  predictions: [],
  city: 'New York'
};

export default TodayCard;
