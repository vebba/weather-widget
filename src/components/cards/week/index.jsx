import React from 'react';
import PropTypes from 'prop-types';
import Time from '../../units/time';
import Icon from '../../icons';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';

const WeekCardWrapper = styled.div`
  ${tw` w-full text-grey mb-4 flex flex-col px-2 md:px-4 md:flex-col items-center`}
`;
const Temperature = styled.div`
  ${tw`font-sans text-1xl text-grey lg:text-xl pb-1`}
`;
const DayTitleWrapper = styled.div`
  ${tw`pb-3 w-full text-center `}
`;

const DayTitle = styled.h2`
  ${tw`font-sans text-xl pb-2 font-normal w-full border-0 border-b border-solid border-grey-dark`}
`;
const PredictionsWrapper = styled.div`
  ${tw`flex  justify-around items-center  w-full `}
`;
const Predictions = styled.div`
  ${tw`flex flex-col items-center pb-2 md:pb-0`}
`;

const WeekCard = props => {
  const { predictions, day } = props;

  if (!predictions.length) {
    return <span>No items in the list</span>;
  }
  return (
    <WeekCardWrapper>
      <DayTitleWrapper>
        <DayTitle>{day}</DayTitle>
      </DayTitleWrapper>
      <PredictionsWrapper>
        {predictions.map(item => (
          <Predictions key={item.dt}>
            <Icon id={item.weather[0]['icon']} />
            <Temperature>{Math.round(item.main['temp'])}&deg;</Temperature>

            <Time time={item.dt_txt} />
          </Predictions>
        ))}
      </PredictionsWrapper>
    </WeekCardWrapper>
  );
};

WeekCard.propTypes = {
  predictions: PropTypes.array.isRequired,
  day: PropTypes.string
};

WeekCard.defaultProps = {
  predictions: [],
  day: 'Monday'
};

export default WeekCard;
