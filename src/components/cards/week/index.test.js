import React from 'react';
import { shallow } from 'enzyme';
import WeekCard from './index';

describe('<WeekCard/>', () => {
  it('should render without props', () => {
    const card = shallow(<WeekCard />);
    expect(card).toMatchSnapshot();
  });
});
