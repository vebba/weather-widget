import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { combineReducers } from 'redux';

import weather from '../containers/weather/reducer';
import search from '../containers/search/reducer';
import mainSaga from '../sagas';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
let middleware = applyMiddleware(sagaMiddleware);
if (process.env.NODE_ENV !== 'production') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
  if (typeof devToolsExtension === 'function') {
    middleware = compose(
      middleware,
      devToolsExtension()
    );
  }
}
export const rootReducer = combineReducers({ weather, search });
const storeInit = createStore(rootReducer, middleware);

// then run the saga
sagaMiddleware.run(mainSaga);

export default storeInit;
