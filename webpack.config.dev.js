const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base');
const Dotenv = require('dotenv-webpack');

module.exports = merge(baseConfig, {
  devtool: 'inline-source-map',
  mode: 'development',
  devServer: {
    port: 8000,
    proxy: {
      '/.netlify/functions': {
        target: 'http://[::1]:9000',
        pathRewrite: {
          '^/.netlify/functions': ''
        }
      }
    }
  },
  plugins: [new Dotenv()]
});
