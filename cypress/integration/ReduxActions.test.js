describe('Redux Actions', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Dispatch getWeather action', () => {
    cy.window()
      .its('store')
      .invoke('dispatch', { type: '@@SEARCH__GET_WEATHER', payload: 'Gdansk' });
  });
});
