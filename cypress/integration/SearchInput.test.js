describe('<SearchInput>', () => {
  beforeEach(() => {
    cy.visit('/');
  });
  it('Accept letters, spaces and dash ', () => {
    const typedText = 'New York';
    cy.get('[data-testid=SearchForm__input]')
      .type(typedText)
      .should('have.value', typedText);
  });
  it('Restrict numbers and special characters', () => {
    const typedText = '123 123  @£$@£$ @£ @£ 2134';
    cy.get('[data-testid=SearchForm__input]')
      .type(typedText)
      .should('have.value', '');
  });
  it('Submit correct city name', () => {
    const typedText = 'New York';
    cy.get('[data-testid=SearchForm__input]').type(typedText);
    cy.get('[data-testid=SearchForm__submit]').click();
  });
  it('Submit incorrect city name', () => {
    const typedText = 'New Yorkerrer';
    cy.get('[data-testid=SearchForm__input]').type(typedText);
    cy.get('[data-testid=SearchForm__submit]').click();
    cy.get('[data-testid=SearchForm__error]').should(
      'have.text',
      'city not found'
    );
  });
});
